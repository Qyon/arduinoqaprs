/*
 * QAPRSTCM3105.cpp
 *
 *  Created on: 23 cze 2014
 *      Author: Admin
 */

#include <QAPRSTCM3105.h>

void QAPRSTCM3105::toggleTone() {
	QAPRSBase::toggleTone();
	if (*(this->pinTone_port) & this->pinTone_bit){
		*(this->pinTone_port) &= ~this->pinTone_bit;
	} else {
		*(this->pinTone_port) |= this->pinTone_bit;
	}
}

void QAPRSTCM3105::initializeRadio() {
	QAPRSBase::initializeRadio();
	this->initControlPins();
}

void QAPRSTCM3105::initControlPins() {
	pinMode(this->toneTxPin,OUTPUT);
	digitalWrite(this->toneTxPin, LOW);
	pinMode(this->toneRxPin,OUTPUT);
	digitalWrite(this->toneRxPin, LOW);
	this->pinTone_port = portOutputRegister(digitalPinToPort(this->toneTxPin));
	this->pinTone_bit = digitalPinToBitMask(this->toneTxPin);
}

void QAPRSTCM3105::init(int8_t sensePin, int8_t txEnablePin, int8_t toneRxPin, int8_t toneTxPin) {
	this->toneTxPin = toneTxPin;
	this->toneRxPin = toneRxPin;
	QAPRSBase::init(sensePin, txEnablePin);
}

void QAPRSTCM3105::init(int8_t sensePin, int8_t txEnablePin, int8_t toneRxPin,
		int8_t toneTxPin, char* from_addr, uint8_t from_ssid, char* to_addr,
		uint8_t to_ssid, char* relays) {
	this->toneTxPin = toneTxPin;
	this->toneRxPin = toneRxPin;
	QAPRSBase::init(sensePin, txEnablePin, from_addr, from_ssid, to_addr, to_ssid, relays);
}

void QAPRSTCM3105::setVariant(QAPRSVariant variant) {
	QAPRSBase::setVariant(QAPRSVHF);
}
