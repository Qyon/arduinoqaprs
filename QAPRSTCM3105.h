/*
 * QAPRSTCM3105.h
 *
 *  Created on: 23 cze 2014
 *      Author: Admin
 */

#ifndef QAPRSTCM3105_H_
#define QAPRSTCM3105_H_

#include <QAPRSBase.h>

class QAPRSTCM3105: public QAPRSBase {
private:
	uint8_t toneTxPin;
	uint8_t toneRxPin;
	uint8_t pinTone_bit;
	volatile uint8_t * pinTone_port;
protected:
	virtual void toggleTone();
	virtual void initializeRadio();
	void initControlPins();
public:
	virtual void init(int8_t sensePin, int8_t txEnablePin, int8_t toneRxPin, int8_t toneTxPin);
	virtual void init(int8_t sensePin, int8_t txEnablePin, int8_t toneRxPin, int8_t toneTxPin, char * from_addr, uint8_t from_ssid, char * to_addr, uint8_t to_ssid, char * relays);
	void setVariant(QAPRSVariant variant);
};

#endif /* QAPRSTCM3105_H_ */
